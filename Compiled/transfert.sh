#####################################################
#                                                   #
# This scripts first cleans the destination folders #
# then transfers the data.                          #
#                                                   #
#####################################################

ssh root@192.168.2.15 rm -rf imu
ssh root@192.168.2.15 mkdir imu
scp lib/* root@192.168.2.15:/usr/bin/.
scp bin/* root@192.168.2.15:imu/.

# antoine.richard@gatech.edu - GeorgiaTech Lorraine 2019
