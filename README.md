# A ROS - EMLID's RTK GPSs IMU Communication Node #

All of the following has been tested an proved to work well on the reach M+, and RS+ only.

## Getting root access after version 2.2X.X

To get root access you will need to manually flash your GPS, the process is not complicated but I guess not recommend by EMLID.
Please **READ** the instructions here https://docs.emlid.com/reachrs/common/reachview/firmware-reflashing/ to learn how to flash the GPS.

Following the instructions in the documentation first download and install the **Reach Firmware Flash Tool**.

**DON'T FORGET TO REBOOT YOUR COMPUTER AS SUGGESTED IN THE GUIDE.**

Then download the lastest image in our case : http://files.emlid.com/images/reach-plus-v2.22.7.zip and unzip it.

Now that the image is here we are going to modify it to add the user *reach* to the sudoers. We will also modify the *sshd_config* so that you can ssh directly as *root* later on.

First, we need to find the offset of the primary partition in the image we have just downloaded. Run :

```
sfdisk Downloads/reach-plus-v2.22.7.img
```

hit `ctrl+c`

In our case we have the following output:

```
Welcome to sfdisk (util-linux 2.31.1).
Changes will remain in memory only, until you decide to write them.
Be careful before using the write command.
Checking that no-one is using this disk right now ... OK
Disk Downloads/reach-plus-v2.22.7.img: 2 GiB, 2189426688 bytes, 4276224 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0xcfa23ff1
Old situation:
Device                            Boot   Start     End Sectors  Size Id Type
Downloads/reach-plus-v2.22.7.img1        49152   81919   32768   16M  c W95 FAT3
Downloads/reach-plus-v2.22.7.img2        81920 2101247 2019328  986M 83 Linux
Downloads/reach-plus-v2.22.7.img3      2113536 4132863 2019328  986M 83 Linux
Downloads/reach-plus-v2.22.7.img4      4145152 4276223  131072   64M 83 Linux
```

Now, the partition we are interested in is the one ending by **.img2**. To modify it, we must mount it. To do so, run the following command and replace START_SECTOR by the starting sector of the partition ending by **.img2**, in our case *81920*.

```
sudo mkdir /mnt/img
sudo mount -o loop,rw,sync,offset=$((512*START_SECTOR)) Downloads/reach-plus-v2.22.7.img /mnt/img
```

So for this specific image we use : 

```
sudo mount -o loop,rw,sync,offset=$((512*81920)) Downloads/reach-plus-v2.22.7.img /mnt/img
```

Now that the image is mounted let's modify it. First, we will modify the sudoers file. It should be located under `/mnt/img/etc/sudoers`. You can edit it using the following command:

```
sudo vim /mnt/img/etc/sudoers
```

The line we are looking for is close to the end of the file.\\
Under the line:
```
root    ALL=(ALL:ALL) ALL
```
add
```
reach    ALL=(ALL:ALL) ALL
```

This image now allows superuser right to the reach user.

**OPTIONALLY** one can also edit the *sshd_config* to log in ssh as root. To do so we need to edit the file located unded `/mnt/img/etc/shh/sshd_config`. From there add `, root` at the end of the very last line. After the modification it should read:
```
AllowUsers reach, root
```

Once you are done modifying the image, unmount it. Run: 
```
sudo umount /mnt/img
```

Your image is now ready to be flashed ! So let's flash ! **BEFORE GOING FURTHER MAKE SURE YOU HAVE REBOOTED YOUR COMPUTER AFTER INSTALLING THE FLASHING TOOL OR THE FLASHING WILL NOT WORK (I TRIED)**

Fire up the flashing software by running:`reach-firmware-flash-tool`. Chose your type of GPS, in our case it's the Reach RS+. Follow the booting procedure (before releasing the button wait for the 3 LED to flash all at the same time: it takes about 10seconds). Once you passed the booting procedure, select your modified image, ignore the warning and flash the device. Wait for the device to reboot. Connect in ssh using the following command: (password: emlidreach)
```
ssh reach@192.168.2.15
```
Then type: (password: emlidreach)
```
sudo su
```
If everything went well you should be root. Now you can add your ssh key, like you usually would. Or check the part of the guide where we do it.


Congrats you are done. The GPS is now under your full control and you can give the middle finger to EMLID.



## Connecting to the GPS in SSH ##

### Accessing the GPS over USB connection ###

Use the `ifconfig` command and look for an over usb ethernet connection, it
should look something like that:
```
enp0s20u1 Link encap:Ethernet  HWaddr 7e:a4:fd:8b:0a:7f  
          inet addr:192.168.2.16  Bcast:192.168.2.255  Mask:255.255.255.0
          inet6 addr: fe80::2da5:5292:17b1:6f43/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:2220 errors:0 dropped:0 overruns:0 frame:0
          TX packets:3697 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000 
          RX bytes:105848 (105.8 KB)  TX bytes:239090 (239.0 KB)
```

Once you have identified which netowrk the GPS is connected to find its IP.
To do so use the `nmap` command. In our case the GPS is connected on the 
**192.168.2.XXX** subnetwork. Thus we use the following command `nmap 192.168.2.*`,
this command will scan all the ip adresses from 192.168.2.1 to 192.168.2.255 and 
report back with all the IP adresses allocated and the ports that are open on them.
You should get the following results:

```
Starting Nmap 7.01 ( https://nmap.org ) at 2019-01-15 09:00 CET
Nmap scan report for 192.168.2.15
Host is up (0.0063s latency).
Not shown: 993 closed ports
PORT     STATE SERVICE
22/tcp   open  ssh
80/tcp   open  http
2000/tcp open  cisco-sccp
2010/tcp open  search
5000/tcp open  upnp
9000/tcp open  cslistener
9001/tcp open  tor-orport

Nmap scan report for 192.168.2.16
Host is up (0.000075s latency).
All 1000 scanned ports on 192.168.2.16 are closed

Nmap done: 256 IP addresses (2 hosts up) scanned in 3.41 seconds
```

My IP address is 192.168.2.16, so the IP of the GPS must be 192.168.2.15. We can
see that its ssh port is open so we will be able to connect to it.

To connect to the GPS we use the `ssh` command.

- **login:** root
- **psswd:** emlidreach

We use the following command: `ssh root@192.168.2.15`

> Note that if you only have on GPS connected **AND NO OTHER USB TO ETHERNET DEVICES**
> you should be able to use the last command directly as the GPS IP seems to be fixed.

### Accessing the GPS over WiFi ###

To access the GPS over WiFi you have two solutions:
- The GPS already connects to a WiFi that you are connected to: You can use the
same step as we did before to access the GPS.
- You want to use the internal WiFi hotspot: You first have to connect to the GPS
with the wire. Then you have to identify the IP of the GPS just like we did before
and connect to the port 80 using your web browser. You should search for 
something like `192.168.2.15:80`. Once this is done you have to edit the WiFi settings
and enable the WiFi hotspot. You will then have to reconnect to the WiFi network of
the GPS and redo the procedure to find the IP of the GPS on that network.

Once either of those two are done you can connect over SSH using the appropriate
IP, login and passwords.

Once this works add your ssh public key to the GPS: this will make the following
operation much easier. In addition to that if you don't do it the rest of our
code won't work.

If you have never used ssh, we will create a pair of ssh keys that allows you 
to connect remotly to the GPS without having to type its password everytime:
- `ssh-keygen`
- hit `enter`
- then hit `enter` till the process is done. Keep in mind that we do this so we
don't have to type a password so when it asks for one press enter don't type
one in !

Now your ssh key pair has been created ! We can add it to the authorized_keys
of the reach GPS.
- `cat .ssh/id_rsa.pub`
- Copy the output of the terminal, it is your public ssh key.
- `ssh root@192.168.2.15 echo PASTE-YOUR-SSH-KEY-HERE >> .ssh/authorized_keys` 
 
## Setting Up the IMU acquisition GPS side ##
Now to the interesting bit: Getting IMU data on the Intel Edison chip.

### RTIMULib ###

For this project we chose to use the RTIMULib as it is a standalone library that
allows easy access to the IMU variables.

You can fin the **precompiled** files for the reach in *Compiled* it
contains:
- The lib in it self under *Compiled/lib*
- The binary that we will be using under *Compiled/bin*

Those file have to be moved to the reach GPS. To do so you can use the provided
script, but we'd recommend doing it manually.
- The libs have to be copied under `/usr/lib/.`. The command should look
something like that: `scp Compiled/lib/* root@IP-ADDRESS:/usr/lib/.`
in our case `scp Compiled/lib/* root@192.168.2.15:/usr/lib/.` feel 
free to use `rsync` instead (`rsync -avP`).
- The Binaries can be where ever you want, yet we'd recommend creating a small
folder to store them in. In our case we could do `ssh root@192.168.2.15 mkdir imu` 
and then send the file using `scp Compiled/bin/* root@192.168.2.15:imu/.` 

Now if access the reach GPS in ssh and run the script we should see IMU values
coming out.
- `ssh root@192.168.2.15`
- `./imu/RTIMULib_simple 100000`

This should result in this:
```
root@reach:~/imu/bin# ./imu/RTIMULib_simple 
Settings file RTIMULib.ini loaded
Using fusion algorithm RTQF
min/max compass calibration not in use
Ellipsoid compass calibration not in use
Accel calibration not in use
MPU-9250 init complete
-1686196381:-0.026123:0.025146:1.020752:-0.026123:0.025146:1.020752
-1686172345:-0.026123:0.021729:1.016357:-0.026123:0.021729:1.016357
-1686150630:-0.028564:0.022949:1.019531:-0.028564:0.022949:1.019531
-1686125223:-0.026855:0.021484:1.015869:-0.026855:0.021484:1.015869
-1686101860:-0.027100:0.020264:1.019287:-0.027100:0.020264:1.019287
-1686075963:-0.028809:0.023193:1.017578:-0.028809:0.023193:1.017578
```

> The very first time that the program is launched you may need to wait a bit
as the configuration file is being created. On the reach M+, we didin't had to
modify that file.

This data is organised in 7 fields, and uses the `:` as a separator, the fields
are organize as follows:
- TimeStamp
- Pose roll
- Pose pitch
- Pose yaw
- Acceleration X
- Acceleration Y
- Acceleration Z


We provide an other more complete binary that integrates several options:
- `ssh root@192.168.2.15`
- `./imu/RTIMULib_advanced -h`
 ```
g : sends the gyroscopes measurements, 3 values: Roll, Pitch, Yaw
a : sends the acceleration measurements, 3 values: X, Y, Z
c : sends the compass measurements, 3 values: Roll, Pitch, Yaw
q : sends the quaternions fusion, 4 values: W, X, Y, Z
f : sampling period in micro seconds (requires a value)
 ```
- `./imu/RTIMULib_advanced -aqcgf 1000000`
```
Settings file RTIMULib.ini loaded
Using fusion algorithm RTQF
min/max compass calibration not in use
Ellipsoid compass calibration not in use
Accel calibration not in use
MPU-9250 init complete
T:1547717452138850
G:-0.001418:0.000581:-0.001492
A:-0.023193:0.018311:1.014404
C:27.051493:-23.549417:2.152493
Q:0.937684:0.004592:0.013115:0.347212
--
T:1547717451599973
G:0.000040:-0.000701:-0.000928
A:-0.018311:0.019043:1.009277
C:26.551804:-23.130087:1.992262
Q:0.937005:0.004460:0.013056:0.349044
--
```
In this mode each letter correspond to a sensor:
- **T**: Timestamp
- **G**: Gyroscopes
- **A**: Accelerometers
- **C**: Compass
- **Q**: Quaternion pose
- **--**: Termination character

### Compiling your own code ###

Because the edison chip has no compilation capacity, we have to perform cross-compilation.
To do so we first have to create a dedicated workspace:

- `mkdir XC-reach`
- `cd XC-reach`

We have then to downloads the compilation tools adapted to the processor that we
want to compile for. To know which precessor you use type `uname -a` on the
target device (GPS). From there we can download the tools directly from the
Linaro servers (one of the main maintaniners of Linux). In our case, our
Processor is an **aarch64** linux processor as such we download the appropriate
compilation packages (
https://releases.linaro.org/components/toolchain/binaries/latest-7/aarch64-linux-gnu/).
- sysroot-glibc-linaro-2.25-2018.05-aarch64-linux-gnu.tar.xz
- gcc-linaro-7.3.1-2018.05-x86\_64\_aarch64-linux-gnu.tar.xz

extract the packages directly into XC-reach using your favorite file extractor
(The mouse works well for those).

Now we have to get the *RTIMULib2* either directly from our repo our from the 
official git `git clone https://github.com/richardstechnotes/RTIMULib2`.

> We made some slight modification to display our messages. And removed all the
unused content.

To compile the lib no matter which one you chose you will need one last thing:
a **TOOL_CHAIN CMAKE FILE**. In XC-reach type in `vim toolchain.cmake` then enter
`i` to get into edit mode. And copy paste the following lines. Don't forget to
edit the path to the *XC-reach* directory.

> We provide our own toolchain.cmake under *scripts*

```
set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR arm)

set(CMAKE_SYSROOT /PATH2/XC-reach)

set(tools /PATH2/XC-reach/gcc-linaro-7.3.1-2018.05-x86_64_aarch64-linux-gnu)
set(CMAKE_C_COMPILER ${tools}/bin/aarch64-linux-gnu-gcc)
set(CMAKE_CXX_COMPILER ${tools}/bin/aarch64-linux-gnu-g++)

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
```

Finally to compile the lib do

- `cd RTIMULib2/Linux/`
- `mkdir build`
- `cd build`
- `cmake .. -DCMAKE_TOOLCHAIN_FILE=PATH2/XC-reach/toolchain.cmake -DCMAKE_INSTALL_PREFIX=PATH2/XC-reach/install `
- `make install`

Normally, the library should be compiled under *PATH2/XC-reach/install*

## Getting the IMU Data on you computer ##

### Through SSH ###

Here instead of using a socket we chose to leverage the SSH protocol and python
easy to use subsystems. One of the advantage of this method is that it doesn't
require any action on the GPS side: Everything can be done directly from your
computer.

To check that everything is properly setted up run the following command:

`ssh root@REACH-IP-ADDRESS PATH2/Executable_on_reach` 

in our case:

`ssh root@192.168.2.15 imu/RTIMULib_simple 100000`

If everything went well you should see the following:

```
Settings file RTIMULib.ini loaded
Using fusion algorithm RTQF
min/max compass calibration not in use
Ellipsoid compass calibration not in use
Accel calibration not in use
MPU-9250 init complete
1547647268548345:-0.025879:0.018311:1.014160:-0.025879:0.018311:1.014160
1547647268571105:-0.024902:0.018555:1.012451:-0.024902:0.018555:1.012451
1547647268595018:-0.021729:0.022461:1.011230:-0.021729:0.022461:1.011230
```

> Note that it may take some time the first time that you launch the script 
by ssh.

If that works well you should be able to start the ros node / python script.
The only thing that is needed is to edit the IP address and path to the script
on the GPS.

Similarily using this command `ssh root@192.168.2.15 imu/RTIMULib_advanced -aqcgf 100000`
should give the following results:
```
Using fusion algorithm RTQF
min/max compass calibration not in use
Ellipsoid compass calibration not in use
Accel calibration not in use
MPU-9250 init complete
T:1547722323924240
G:-0.006550:0.000654:0.003764
A:-0.019287:0.013428:1.015869
C:27.839367:-25.968525:3.617117
Q:0.928811:0.000853:0.011267:0.370381
--
T:1547722324926782
G:-0.000065:0.000260:0.000281
A:-0.019775:0.017334:1.014404
C:26.711063:-26.140596:1.940055
Q:0.928736:0.003220:0.011996:0.370534
--
```

> Note that it may take some time the first time that you launch the script 
by ssh.

### Through ROS ###
copy paste our ROS-package *reach-imu* compile the package and that's it. 
Use either `roslaunch reach-imu advanced_imu.launch` or `roslaunch reach-imu simple_imu.launch`.

#### simple-imu ####

#### advanced-imu ####

### Through a Python script ###
The python scripts redirects the stdout from the c++ executable to a python 
variable. In the python script make sure to edit the *IP*, and *BIN_PATH*
variables so that they match your set up.
> The python code does not include a translator from the messages that the IMU
sents. To get a feeling of how it could be done have a look at how our ROS 
code does it *ROS/reach-imu/src/reach-imu.py* and *ROS/reach-imu/src/reach-imu-advanced.py*. 

