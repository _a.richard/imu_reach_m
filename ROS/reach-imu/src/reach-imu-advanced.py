#!/usr/bin/env python

import subprocess
import rospy
import numpy as np
from sensor_msgs.msg import Imu
from sensor_msgs.msg import MagneticField


# TODO ==> GET REFERENCE TIME FROM GPS
# NTP --> GET CLOCK FROM GPS REFERENCE.

class IMU_Receiver:
    def __init__(self):
        # Launch params
        self.IP = rospy.get_param('~IP', 'root@192.168.2.15')
        self.EX_PATH = rospy.get_param('~bin_path', '/home/root/imu/RTIMULib_advanced')
        self.imu_args = rospy.get_param('~imu_args', 'agq')
        self.get_compass = rospy.get_param('~get_compass', 'True')
        self.freq = rospy.get_param('~frequency', 10)
        self.frame = rospy.get_param('~frame', 'Imu')
        self.time = rospy.get_param('~time', 'ROS')

        # Binary arguments
        self.args = '-'+self.imu_args
        self.freq = int((1.0/self.freq)*1000000)
        
        # Time configuration (ROS or GPS)
        if self.time == 'ROS':
            self.time = 0
        else:
            self.time = 1
        
        # Messages
        self.imu = Imu()
        self.mf = MagneticField()

        # Publishers
        # IMU
        if self.imu_args != 'None':
            self.imu_pub_ = rospy.Publisher('imu', Imu, queue_size=1)
        else:
            self.args = ""
        # Magnetometer
        if self.get_compass:
            self.args += 'cf'
            self.mf_pub_ = rospy.Publisher('magnetic_field', MagneticField, queue_size=1)
        else:
            self.args += 'f'
        print self.args

    def process(self, string):
        processed = string.split(':')
        if processed[0] == 'T':
            self.imu.header.frame_id = self.frame
            self.mf.header.frame_id = self.frame
            if self.time == 0:
                self.imu.header.stamp = rospy.Time.now()
                self.mf.header.stamp = rospy.Time.now()
            elif self.time == 1:
                self.imu.header.stamp = int(processed[1])
                self.mf.header.stamp = int(processed[1])
        elif processed[0] == 'Q':
            self.imu.orientation.w = float(processed[1])
            self.imu.orientation.x = float(processed[2])
            self.imu.orientation.y = float(processed[3])
            self.imu.orientation.z = float(processed[4])
        elif processed[0] == 'A': 
            self.imu.linear_acceleration.x = float(processed[1])
            self.imu.linear_acceleration.y = float(processed[2])
            self.imu.linear_acceleration.z = float(processed[3])
        elif processed[0] == 'G':
            self.imu.angular_velocity.x = float(processed[1])
            self.imu.angular_velocity.y = float(processed[2])
            self.imu.angular_velocity.z = float(processed[3])
        elif processed[0] == 'C':
            self.mf.magnetic_field.x = float(processed[1])
            self.mf.magnetic_field.y = float(processed[2])
            self.mf.magnetic_field.z = float(processed[3])
        else:
            if self.imu_args != 'None':
                self.imu_pub_.publish(self.imu)
            if self.get_compass:
                self.mf_pub_.publish(self.mf) 
     
    def run(self):
        com = subprocess.Popen(['ssh', self.IP, self.EX_PATH, self.args, str(self.freq)], stdout=subprocess.PIPE)
        # Skip process status
        data = com.stdout.readline().rstrip()
        data = com.stdout.readline().rstrip()
        data = com.stdout.readline().rstrip()
        data = com.stdout.readline().rstrip()
        data = com.stdout.readline().rstrip()
        data = com.stdout.readline().rstrip()
        while not rospy.is_shutdown():
            # Records data
            data = com.stdout.readline().rstrip()
            self.process(data)

if __name__ == '__main__':
    rospy.init_node('imu_network_hook', anonymous=True)
    IMU = IMU_Receiver()
    IMU.run()

# antoine.richard@gatech.edu - GeorgiaTech Lorraine 2019
