#!/usr/bin/env python

import subprocess
import rospy
import numpy as np
from sensor_msgs.msg import Imu


# TODO ==> GET REFERENCE TIME FROM GPS
# NTP --> GET CLOCK FROM GPS REFERENCE.

class IMU_Receiver:
    def __init__(self):
        self.IP = rospy.get_param('~IP', 'root@192.168.2.15')
        self.EX_PATH = rospy.get_param('~bin_path', '/home/root/imu/RTIMULib_Acc_Gyro')
        self.freq = rospy.get_param('~frequency', 10)
        self.freq = int((1.0/self.freq)*1000000)
        self.imu_pub_ = rospy.Publisher('imu', Imu, queue_size=1)

    def process_string(self, string):
        processed = string.split(':')
        TS = int(processed[0])
        for i in range(1,7):
            processed[i] = float(processed[i])
        A = processed[4:7]
        Q = self.toQuaternion(processed[1], processed[2], processed[3])
        return TS, A, Q

    def toQuaternion(self, roll, pitch, yaw):
        # Abbreviations for the various angular functions
        cy = np.cos(yaw * 0.5)
        sy = np.sin(yaw * 0.5)
        cp = np.cos(pitch * 0.5)
        sp = np.sin(pitch * 0.5)
        cr = np.cos(roll * 0.5)
        sr = np.sin(roll * 0.5)
    
        qw = cy * cp * cr + sy * sp * sr
        qx = cy * cp * sr - sy * sp * cr
        qy = sy * cp * sr + cy * sp * cr
        qz = sy * cp * cr - cy * sp * sr
        return [qw, qx, qy, qz]
    
     
    def run(self):
        # Sleeps for a while to initialiaze GPS connection
        com = subprocess.Popen(["ssh",self.IP,self.EX_PATH, str(self.freq)], stdout=subprocess.PIPE)
        data = com.stdout.readline().rstrip()
        data = com.stdout.readline().rstrip()
        data = com.stdout.readline().rstrip()
        data = com.stdout.readline().rstrip()
        data = com.stdout.readline().rstrip()
        data = com.stdout.readline().rstrip()
        while not rospy.is_shutdown():
            data = com.stdout.readline().rstrip()
            TS, A, Q = self.process_string(data)
            msg = Imu()
            msg.header.frame_id = 'Imu'
            msg.header.stamp = rospy.Time.now()
            msg.orientation.w = Q[0]
            msg.orientation.x = Q[1]
            msg.orientation.y = Q[2]
            msg.orientation.z = Q[3]
            msg.linear_acceleration.x = A[0]
            msg.linear_acceleration.y = A[1]
            msg.linear_acceleration.z = A[2]
            self.imu_pub_.publish(msg)

if __name__ == '__main__':
    rospy.init_node('imu', anonymous=True)
    IMU = IMU_Receiver()
    IMU.run()

# antoine.richard@gatech.edu - GeorgiaTech Lorraine 2019
